# Ancien
Cette documentation d’installation est basé Debian avec apache2, mariadb, et php-mod. Vous pouvez l'adapter selon votre OS et vos logiciels.

Dans les étapes suivantes, il sera considéré que vous êtes root, si vous ne l'êtes pas, adaptez vos commandes.

## Installation guidée

### Basique

Définir des mots de passe aléatoire
```bash
my_domain="Your.Domain.tld"

sec_admin_pwd=$(openssl rand -base64 18)
echo $sec_admin_pwd > /root/.sec_admin_pwd.txt

sec_db_pwd=$(openssl rand -base64 18)
echo $sec_db_pwd > /root/.sec_db_pwd.txt
``` 

Définir le hostname
```bash
hostnamectl set-hostname $my_domain
```

Mettre a jour votre système
```bash
apt update && apt full-upgrade -y
```

Installer les paquets nécessaires
```bash
apt install -y apache2 libapache2-mod-php php-common mariadb-server php-json php-mysql php-gd wget
```

### Serveur web

Créer le fichier de configuration apache2
```bash
FILE="/etc/apache2/sites-available/andevie.conf"
cat <<EOM >$FILE
<VirtualHost *:80>
	DirectoryIndex index.php index.html
	DocumentRoot /var/www/andevie
	<Directory /var/www/andevie>
		Options +FollowSymlinks -Indexes
		AllowOverride All
		Require all granted
		SetEnv HOME /var/www/andevie
		SetEnv HTTP_HOME /var/www/andevie
	</Directory>
</VirtualHost>
EOM
```

Activer ce site
```bash
a2dissite 000-default
a2ensite andevie.conf
```

Activer les modules nécessaires
```bash
a2enmod dir env headers mime rewrite setenvif
systemctl restart apache2
```

### Base de données

Penser a faire une installation sécurisée grâce à `mysql_secure_installation`

Puis faire ça
```bash
sed -i "/\[mysqld\]/atransaction-isolation = READ-COMMITTED\nperformance_schema = on" /etc/mysql/mariadb.conf.d/50-server.cnf
systemctl start mariadb
```

### Instalation

```bash
cd /var/www/
wget https://gitlab.com/ecojetons/app-economie-jetons-collaborative/-/archive/latest/app-economie-jetons-collaborative-latest.tar.gz && \
tar -xf app-economie-jetons-collaborative-latest.tar.gz && \
mv app-economie-jetons-collaborative-latest/ andevie/ && \
chown -R www-data andevie
```

Lancer les scripts d'initialisation de la BD

Création de la BD et de l'utilisateur
```bash
mysql -u root -e "
CREATE DATABASE IF NOT EXISTS andevie; \
GRANT ALL PRIVILEGES ON andevie.* TO andevie@localhost IDENTIFIED BY '${sec_db_pwd}';"
```

Initialisation des tables
```bash
mysql -u root -D andevie < /var/www/andevie/db/createTables.sql
```

Creer le fichier de config du site
```bash
FILE="/var/www/andevie/config.json"
cat <<EOM >$FILE
{
  "BaseDonnees": {
    "Systeme": "mysql",
    "Hote": "localhost",
    "Nom": "andevie",
    "Utilisateur": "andevie",
    "MotDePasse": "$sec_db_pwd"
  },
  "WebSocket": {
    "Protocole": "wss://",
    "Domaine": "$my_domain",
    "Chemin": "/wss",
    "Port": "",
    "PortInterne": 8888
  }
}
EOM
```

Lancer le Websocket
```bash
php websockets/run.php
```

Création de l'administrateur (premier compte)
```bash
hash_passwd=$(php -r "echo password_hash($sec_admin_pwd,PASSWORD_DEFAULT);")
mysql -u root -D andevie -e "
INSERT INTO membre (idmembre, nom,prenom,adresse,codepostal,ville,courriel,datenaissance,mdp,admin,comptevalide) VALUES 
(1, "administratrice", "administrateur", "ecojetons", "00000", "n/a", "admin@ecojetons", "1970-01-01", "$sec_admin_pwd", 1, 1);"
```

## Finition

Revérifier que les permissions sont bonnes
```bash
cd /var/www/
chown -R www-data. andevie
```

Afficher les informations
```bash
echo "Your Admin password is: "$sec_admin_pwd
echo "It's documented at /root/.sec_admin_pwd.txt"
echo "Your Database Password is: "$sec_db_pwd
echo "It's documented at /root/.sec_db_pwd.txt"
echo "Your andevie is accessable under: "$my_domain
echo "The Installation is complete."
```