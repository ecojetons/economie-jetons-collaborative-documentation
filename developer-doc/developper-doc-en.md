# Developer documentation

## API

### How to acces?,  What they send?

#### Roles
```
api/roles {GET;POST}
api/roles {GET;PUT;DELETE}
```
```json
{
	"id_role":1,
	"entilted":"entilted"
}
```

#### Children
```
api/children {GET;POST}
api/children/id {GET;DELETE;PUT}
```
```json
{
	"id_child":1,
	"name":"name",
	"surname":"surname",
	"birthdate":"01/01/2001",
	"token":"?",
	"objectives":[1,2,3],
	"rewards":[1,2,3]
}
```

#### Team-members
##### Team-member of a child
```
api/children/id/team-members {GET;POST(add a member to a team)}
api/children/id/team-members/id(member) {DELETE(eject of a team);PATCH}
```
```json
{
	"member": {
		"id_member":1,
		"name":"name",
		"surname":"surname",
		"birthdate":"01/01/2001",
		"email":"email@ecojeton.com",
		"address":"address",
		"zip_code":"31000",
		"city":"city",
		"validity":1,
		"admin":1
	},
	"id_role":1,
	"date_request":"01/01/2001",
	"validity":0,
}
```

##### Team of member
```
api/members/id/team-members {GET;POST}
api/members/id/team-members/id(child) {DELETE(eject of a team);PUT}
```
```json
{
	"child":{
		"id_child":1,
		"name":"name",
		"surname":"surname",
		"birthdate":"01/01/2001",
		"token":"?",
		"objectives":[1,2,3],
		"rewards":[1,2,3]
	},
	"id_role":1,
	"date_request":"01/01/2001",
	"validity":0,
}
```

#### Rewards
##### Rewards of child
```
api/children/id/rewards {GET;POST}
api/children/id/rewards/id {GET;DELETE;PUT}
```
```json
{
	"id_reward":1,
	"entitled":"entitled",
	"description":"description",
	"image": "☣",
	"objective":[1,2,3]
}
```

##### Reward of objective
```
api/children/id/objectives/id/rewards {GET;POST(add a newly created reward)}
api/children/id/objectives/id/rewards/id {GET;DELETE;PUT}
```
```json
{
	"id_reward":1,
	"entitled":"entitled",
	"description":"description",
	"image": "?"
}
```

#### Objectives
```
api/children/id/objectives {GET;POST}
api/children/id/objectives/id {GET;DELETE;PUT}
```
```json
{
	"id_objective":1,
	"entitled":"entitled",
	"success_rate":0.7,
	"time":5000
}
```

#### Situations
```
api/children/id/objectives/id/situations {GET;POST}
api/children/id/objectives/id/situations/id {GET}
```
```json
{
	"id_situation":1,
	"nbr_max_token":20,
	"nbr_token":11,
	"time":5000,
	"remaining_time":2000,
	"status":"w"
}
```

#### Tokens
```
api/children/id/objectives/id/situations/id/tokens {GET;POST(add ou del)}
```
```json
{
	"id_situation":1,
	"timestamp":"01/01/2001 10:30:20",
	"add":1,
	"id_member":1
}
```

#### Member
```
api/members {GET;POST}
api/members/id {GET;DELETE;PUT}
```
##### Logged as administrator
```json
{
	"id_member":1,
	"name":"name",
	"surname":"surname",
	"birthdate":"01/01/2001",
	"email":"email@ecojeton.com",
	"address":"address",
	"zip_code":"31000",
	"city":"city",
	"coordinator":1,
	"validity":1,
	"admin":1
}
```
##### Other
```json
{
	"id_member":1,
	"name":"name",
	"surname":"surname",
	"birthdate":"01/01/2001",
	"email":"email@ecojeton.com",
	"address":"address",
	"zip_code":"31000",
	"city":"city",
}
```

### Mandatory access control

#### Child

##### Follower

- GET followed children
- GET a specific followed child

##### Coordinator

- GET coordinated children
- GET a specific coordinated child
- PATCH image_token of a specific coordinated child

##### Administrator

- POST a child
- PUT a specific child
- GET every child
- GET a specific child
- DELETE a specific child

#### Member

##### Himself

- PATCH his personal information
- GET himself

##### Administrator

- POST a child
- PUT a specific child
- GET every child
- GET a specific child
- DELETE a specific child

#### Role

##### User

- GET every role

##### Administrator

- POST a role
- PUT a specific role
- GET every role
- GET a specific role
- DELETE a specific role

#### Objective of a specific child

##### Follower of child

- GET every objective
- GET a specific objective

##### Coordinator of child

- POST an objective
- PATCH a specific objective
- GET every objective
- GET a specific objective
- DELETE a specific objective

##### Administrator

- POST an objective
- PUT a specific objective
- GET every objective
- GET a specific objective
- DELETE a specific objective

#### Reward of a child

##### Follower of child

- GET every reward
- GET a specific reward

##### Coordinator of child

- POST a reward
- PATCH a specific reward
- GET every reward
- GET a specific reward
- DELETE a specific reward

##### Administrator

- POST a reward
- PUT a specific reward
- GET every reward
- GET a specific reward
- DELETE a specific reward

#### Situation of an objective of a child

##### Follower of child

- POST a situation
- GET every situation
- GET a specific situation

##### Coordinator of child

- POST a situation
- GET every situation
- GET a specific situation
- DELETE a specific situation

##### Administrator

- POST a situation
- GET every situation
- GET a specific situation
- DELETE a specific situation

#### Reward of an objective of a child

##### Follower of child

- GET every reward
- GET a specific reward

##### Coordinator of child, Administrator

- POST a reward
- GET every reward
- GET a specific reward
- DELETE a specific reward

#### Token of a situation of an objective of a child

##### Follower or coordinator of child, administrator

- POST a token
- GET every token
- DELETE a token

#### Team-member of a child

##### Follower of child

- GET every team-members (the team)

##### Coordinator of child, Administrator

- POST a team-member
- PATCH a specific team-member (change role)
- GET every team-members
- DELETE a specific team-member (eject)

#### Team-member (a member)

##### Himself

- GET every team-member (followings of children)

##### Administrator

- GET every team-member of a specific member

### Authentication

The JWT token body is in the form :
``` json
{
	"id_member":42,
	"admin":1,
	"coordinator":1
}
```
with admin & coordinator being informational as how to display interface.
