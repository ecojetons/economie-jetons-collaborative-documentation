# User Manual

## Preface

This manual is for all  users of the system, we start by looking at the different roles available, from the one with the less responsibilities to the one with the most responsibilities:

* member
* coordinator
* administrator

![Schéma des rôles](https://test.com "Schéma des rôles")

## Member

This section of the manual explains the interface and the basic fonctionnalities that you can access as a member, who can be either be a parent, a professor or a medical staff

The main actions possible are:

* Modify your profil
* Communicate with other members
* Evaluate a child

### Home page

![Capture d'écran de la page d’accueil](https://test.com "Capture d'écran de la page d’accueil")

1. Profil tab: Permits you to change your personnal information
2. Disconnect button : Allows you to disconnect 
3. Messagerie tab : Permits you to discuss with other members
4. Tab suivi : List of the children you taking care of 
5. Search bar : Permits you to search information on the page

### Profil tab

![Capture d'écran de la page profil](https://test.com "Capture d'écran de la page profil")

This page enables you to change your connexion information (email, password) and personnal information (name, surname, birthday, address)

### Messagerie tab

![Capture d'écran de la messagerie](https://test.com "Capture d'écran de la messagerie")

1. Enfant : select a child

![Capture d'écran de la messagerie, enfant sélectionné](https://test.com "Capture d'écran de la messagerie, enfant sélectionné")

1. Enfant : select a child
2. Objectif : select an objective

![Capture d'écran de la messagerie, chat ouvert](https://test.com "Capture d'écran de la messagerie, chat ouvert")

1. Enfant : select a child
2. Objectif : select an objective
3. Messagerie : you can send and receive messages in this section

### Suivi tab

This is the main page, it allows you to evaluate and see the advancement and information of the child

![Capture d'écran de la page suivi](https://test.com "Capture d'écran de la page suivi")

1. Enfant : clic on a child's name to see more information
2. Search bar : You can research by name, surname, birth date and any other information

![Capture d'écran de la page suivi, enfant déplié](https://test.com "Capture d'écran de la page suivi, enfant déplié")

1. Enfant Information : The information of the child (name, surname, birth date...)
2. Objectif tab : Tab about the child's objectives
3. Équipe tab : Tab about the child's team
4. Récompense tab: Tab about the child's reward

#### Objectif tab

![Capture d'écran de la page suivi, enfant déplié, onglet objectif](https://test.com "Capture d'écran de la page suivi, enfant déplié, onglet objectif")

1. Active Objectif : The objectives written in black can be evaluate

2. Objective disable : the objectives written in grey have been disabled by an administrator or a coordinator, contact them for more information
   
   

![Capture d'écran de la page suivi, enfant déplié, onglet objectif déplié](https://test.com "Capture d'écran de la page suivi, enfant déplié, onglet objectif déplié")

1. nouvelle situation button :  permits to create a new learning situations
2. Situation button :  permits to access to previous learning situations

#### Équipe tab

![Capture d'écran de la page suivi, enfant déplié, onglet équipe](https://test.com "Capture d'écran de la page suivi, enfant déplié, onglet équipe")

This tab introduces the child's team members.

#### Récompenses tab

![Capture d'écran de la page suivi, enfant déplié, onglet récompense](https://test.com "Capture d'écran de la page suivi, enfant déplié, onglet récompense")

This tab introduces the child's rewards.

## Coordinator



This section of the manual explains the inferfaces and the fonctionnalities that you can access as a coordinator (a coordinator is a person in charge of a child) in addition, who also has the same rights as a member

The exclusive actions that you can do are the following : 

* The management a child's rewards (create/edit/delete)
* The management of a child's team(accept the follow-up request/reject the follow-up request/eject a member)
* Edit the token image of a child

### Coordination tab

![Capture d'écran de la page coordination](https://test.com "Capture d'écran de la page coordination")
The coordination page is close to the "suivi" page, it has the same structure and interfaces.

![Capture d'écran de la page coordination, enfant déplié](https://test.com "Capture d'écran de la page coordination, enfant déplié")

This view is similar to the suivi one, but it provide edit options on all the 3 tabs:

* Objectif
* Équipe
* Récompense

#### Objectif tab

![Capture d'écran de la page coordination, enfant déplié, onglet objectif](https://test.com "Capture d'écran de la page coordination, enfant déplié, onglet objectif")

1. Editing button

2. Add button

3. Activation Slider
   
   If you click on an objective it goes into edit mode

![Capture d'écran de la page coordination, enfant déplié, onglet objectif, mode modification](https://test.com "Capture d'écran de la page coordination, enfant déplié, onglet objectif, mode modification")

1. Suppresion button
2. Editable field

#### Équipe tab

![Capture d'écran de la page coordination, enfant déplié, onglet équipe](https://test.com "Capture d'écran de la page coordination, enfant déplié, onglet équipe")

1. Eject of the team button
2. Add to the team button

#### Récompense tab

![Capture d'écran de la page coordination, enfant déplié, onglet récompense](https://test.com "Capture d'écran de la page coordination, enfant déplié, onglet récompense")

1. Delete button
2. Add button

## Administrator

This section of the manual explains the interfaces and the fonctionnalities of the administrator part, this is the role with the largest range of permittion, you have to be aware, great power implies great responsibilities. 

The main actions that you can execute, in addition to all the previous ones, are:

* The member's management (create/edit/delete/validate)
* Children's management (création/modification/suppression)

### Membres page

![Capture d'écran de la page membre](https://test.com "Capture d'écran de la page membre")

1. Add button
2. Members

If you click on a member, you can access to more options

![Capture d'écran de la page membre, membre déplié](https://test.com "Capture d'écran de la page membre, membre déplier")

1. Objectifs proposés tab
2. Enfants suivis tab
3. Activation slider 
4. Editable information

#### Objectif proposés tab

![Capture d'écran de la page membre, membre déplié, onglet objectif proposés](https://test.com "Capture d'écran de la page membre, membre déplié, onglet objectif proposés")

#### Enfant suivis tab

![Capture d'écran de la page membre, membre déplié](https://test.com "Capture d'écran de la page membre, membre déplié, onglet objectif proposés")

1. Add button

### Enfant page

![Capture d'écran de la page enfant](https://test.com "Capture d'écran de la page enfant")

1. Add button
2. Enfants

If you click on a children, you can access more options

![Capture d'écran de la page enfant, enfant déplié](https://test.com "Capture d'écran de la page enfant, enfant déplié")

1. Delete button
2. Editable information
3. Objective tab
4. Team tab
5. Reward tab

#### Objectif tab

![Capture d'écran de la page enfant, enfant déplié, onglet objectif](https://test.com "Capture d'écran de la page enfant, enfant déplié, onglet objectif")

This tab is the same as the coordination one, refere to coordinator part.

#### Équipe tab

![Capture d'écran de la page enfant, enfant déplié, onglet équipe](https://test.com "Capture d'écran de la page enfant, enfant déplié, onglet équipe")

This tab is the same as the coordination one, please look up at the coordinator section.

#### Récompense tab

![Capture d'écran de la page enfant, enfant déplié, onglet récompense](https://test.com "Capture d'écran de la page enfant, enfant déplié, onglet récompense")

This tab is the same as the coordination one, please look up to coordinator part.