# Manuel Utilisateur

## Préface

Ce manuel s'adresse à tous les utilisateur du système, Nous allons commencer par voir les différents rôles disponibles, du rôle avec le moins de responsabilité au plus de responsabilité:

* membres
* coordinateur
* administrateur

![Schéma des rôles](https://test.com "Schéma des rôles")

## Membre

Cette partie du manuel vas vous expliquer l'interface et les fonctionnalité de base que vous pouvez effectuer en tant que membres, c'est à dire un parent, un professeur, un personnel médical...

Les actions principales que vous pouvez effectuer sont:

* la modification de son profil
* la communication avec les autres membres
* la notation d'un enfant

### Page d’accueil

![Capture d'écran de la page d’accueil](https://test.com "Capture d'écran de la page d’accueil")

1. Onglet Profil : Vous permet de changer vos informations personnelles
2. Bouton Déconnexion : Vous permet de vous déconnectez
3. Onglet Messagerie : Vous permet de discuter avec les autres utilisateurs
4. Onglet Suivi : Liste des enfant dont vous vous occupez
5. Barre de Recherche : vous permet de recherchez quelque chose dans la page actuel

### Page Profil

![Capture d'écran de la page profil](https://test.com "Capture d'écran de la page profil")

Cette page vous permet de changer vos information de connexion (mail, mot de passe) et personnelles (nom, prénom, date de naissance, adresse)

### Page Messagerie

![Capture d'écran de la messagerie](https://test.com "Capture d'écran de la messagerie")

1. Enfant : sélectionnez l'enfant

![Capture d'écran de la messagerie, enfant sélectionné](https://test.com "Capture d'écran de la messagerie, enfant sélectionné")

1. Enfant : sélectionnez l'enfant
2. Objectif : sélectionnez l'objectif

![Capture d'écran de la messagerie, chat ouvert](https://test.com "Capture d'écran de la messagerie, chat ouvert")

1. Enfant : sélectionnez l'enfant
2. Objectif : sélectionnez l'objectif
3. Zone de discussion : vous pouvez envoyer et recevoir des messages en direct à partir de cette zone 

### Page Suivi

Cette page est la page principale, elle vous permet d'évaluer, de voir l'évolution de l'enfant et tous les information autour de ce dernier

![Capture d'écran de la page suivi](https://test.com "Capture d'écran de la page suivi")

1. Enfant : cliquez sur un enfant pour plus d'information
2. Barre de recherche : vous pouvez rechercher

![Capture d'écran de la page suivi, enfant déplié](https://test.com "Capture d'écran de la page suivi, enfant déplié")

1. Information Enfant : les informations de l'enfant (nom, prénom, date de naissance...)
2. Onglet Objectif : Onglet concernant les objectifs de l'enfant
3. Onglet Équipe : Onglet concernant l'équipe de l'enfant
4. Onglet Récompense : Onglet concernant les récompenses de l'enfant

#### Onglet Objectif

![Capture d'écran de la page suivi, enfant déplié, onglet objectif](https://test.com "Capture d'écran de la page suivi, enfant déplié, onglet objectif")

1. Objectif Actif : Les objectifs écrit en noir peuvent être évalué
2. Objectif Désactiver : Les objectifs écrit en gris ont été désactivé par un administrateur ou un coordinateur, contactez les pour plus d'information

![Capture d'écran de la page suivi, enfant déplié, onglet objectif déplié](https://test.com "Capture d'écran de la page suivi, enfant déplié, onglet objectif déplié")

1. Bouton nouvelle situation : permet de créer une nouvelle situation d'apprentissage
2. Bouton Situation : permet d’accéder à une ancienne situation d'apprentissage

#### Onglet Situation

![Capture d'écran de la page situation](https://test.com "Capture d'écran de la page situation")

1. L'objectif avec la progression actuelle de l'enfant.
2. Bouton ajouter/retirer un point : permet d'ajouter ou de retirer un point, apres un ajout ou un retrer une attente de quelque seconde est nécessaire.
3. Temps restant : le temps qu'il reste avent que l'objectf se termine.

#### Onglet Équipe

![Capture d'écran de la page suivi, enfant déplié, onglet équipe](https://test.com "Capture d'écran de la page suivi, enfant déplié, onglet équipe")

Cet onglet présente les membres de l'équipe de l'enfant

#### Onglet Récompenses

![Capture d'écran de la page suivi, enfant déplié, onglet récompense](https://test.com "Capture d'écran de la page suivi, enfant déplié, onglet récompense")

Cet onglet présente la liste de récompense que l'enfant peut obtenir.

## Coordinateur

Cette partie du manuel vas vous expliquer l'interface et les fonctionnalité que vous pouvez effectuer en tant que coordinateur, c'est à dire le responsable d'un enfant, de plus en tant que coordinateur vous pouvez aussi effectuer les actions d'un membre (vue précédemment).

Les actions exclusive principales que vous pouvez effectuer sont:

* la gestion des récompenses d'un enfant(création/modification/suppression)
* la gestion de l'équipe d'un enfant(accepter demande/refuser demande/éjecter un membre)
* changer l'image de jeton d'un enfant

### Page Coordination

![Capture d'écran de la page coordination](https://test.com "Capture d'écran de la page coordination")
La page coordination est très proche de la page suivi, en effet elle possède la même structure, la même interface

![Capture d'écran de la page coordination, enfant déplié](https://test.com "Capture d'écran de la page coordination, enfant déplié")

Cette vue est très similaire à la celle de suivi, si ce n'est qu'elle dispose des options de modification sur les 3 onglets

* Objectif
* Équipe
* Récompense

#### Onglet Objectif

![Capture d'écran de la page coordination, enfant déplié, onglet objectif](https://test.com "Capture d'écran de la page coordination, enfant déplié, onglet objectif")

1. Objectif clicable pour modification
2. Bouton ajout

![Capture d'écran de la page coordination, enfant déplié, onglet objectif, mode modification](https://test.com "Capture d'écran de la page coordination, enfant déplié, onglet objectif, mode modification")

1. Bouton suppression
2. Champs modifiable

#### Onglet Équipe

![Capture d'écran de la page coordination, enfant déplié, onglet équipe](https://test.com "Capture d'écran de la page coordination, enfant déplié, onglet équipe")

1. Bouton retirer de l'équipe

#### Onglet Récompense

![Capture d'écran de la page coordination, enfant déplié, onglet récompense](https://test.com "Capture d'écran de la page coordination, enfant déplié, onglet récompense")

1. Bouton supprimer : pour y accéder il faut avoir au préalable cliqué sur le bouton modifier.
2. Bouton ajout
3. Bouton modifier

## Administrateur

Cette partie du manuel vas vous expliquer l'interface et les fonctionnalités de la partie administrateur, il s'agit du "rôle" avec le plus de pouvoir, il faut donc être prudent, un grand pouvoir implique de grande responsabilité. Ce rôle correspond plus au secrétariat, 

Les actions exclusive principale principales que vous pouvez effectuer sont:

* la gestion des membres (création/modification/suppression/validation)
* la gestion des enfants (création/modification/suppression)

### Page Membres

![Capture d'écran de la page membre](https://test.com "Capture d'écran de la page membre")

1. Bouton ajout
2. Membres

Si on clique sur un membre on peut accéder à plus d'options

![Capture d'écran de la page membre, membre déplié](https://test.com "Capture d'écran de la page membre, membre déplier")

1. Onglet objectifs proposés
2. Onglet enfants suivis
3. Slider activation
4. Informations modifiables

#### Onglet objectif proposés

![Capture d'écran de la page membre, membre déplié, onglet objectif proposés](https://test.com "Capture d'écran de la page membre, membre déplié, onglet objectif proposés")

#### Onglet enfant suivis

![Capture d'écran de la page membre, membre déplié](https://test.com "Capture d'écran de la page membre, membre déplié, onglet objectif proposés")

1. Bouton ajout

### Page Enfant

![Capture d'écran de la page enfant](https://test.com "Capture d'écran de la page enfant")

1. Bouton d'ajout
2. Enfants

Si on clique sur un enfant on peut accéder à plus d'option

![Capture d'écran de la page enfant, enfant déplié](https://test.com "Capture d'écran de la page enfant, enfant déplié")

1. Bouton supprimer
2. Informations modifiables
3. Onglet Objectif
4. Onglet Équipe
5. Onglet Récompense

#### Onglet Objectif

![Capture d'écran de la page enfant, enfant déplié, onglet objectif](https://test.com "Capture d'écran de la page enfant, enfant déplié, onglet objectif")

Cet onglet est le même que dans coordination, se référer à la partie coordinateur

#### Onglet Équipe

![Capture d'écran de la page enfant, enfant déplié, onglet équipe](https://test.com "Capture d'écran de la page enfant, enfant déplié, onglet équipe")

Cet onglet est le même que dans coordination, se référer à la partie coordinateur

#### Onglet Récompense

![Capture d'écran de la page enfant, enfant déplié, onglet récompense](https://test.com "Capture d'écran de la page enfant, enfant déplié, onglet récompense")

Cet onglet est le même que dans coordination, se référer à la partie coordinateur